//CARGA DE DEPENDENCIAS
var express = require("express");
var app = express();

var bodyparser = require("body-parser");

var mongoose = require ("mongoose");
const req = require("express/lib/request");

//USO DE DEPENDENCIAS
app.use (express.json());
app.use(express.urlencoded({
    extended: true
}));

//CONFIGUARACION DE LA CABECERA Y CORS
app.use((req,res, next)=>{
    res.header(`Access-Control-Allow-Origin`,`*`);
    res.header(
        "access-control-allow-headers",
        "authorization, X-API-KEY, origin, X-Requested-With, content-type, accept, Access-Control-Allow-Request-Method"
      );
    res.header(`Access-Control-Allow-Request-Method`,`GET,POST,PUT,DELETE,OPTIONS`);
    next ();
});
//RUTAS
app.use(require(`./routers/routers`));
module.exports = app;

