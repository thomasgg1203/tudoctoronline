const mongoose =require(`mongoose`);
//Direccion de la ruta de mongoDB
mongoose.connect("mongodb://localhost:27017/db_tu_doctor_online",{
    useNewUrlparser:true,
    //useCreateIndex:true,
    useUnifiedTopology:true,
    //useFindAndModify:false
},(err,res)=>{
    if(err){
        throw err //si no se genera la conexion correctamente
    }else{
        console.log(`la CNX a la BD se realizo de forma correcta`);//Cuando se genere correctamente
    }
});
module.exports=mongoose;
