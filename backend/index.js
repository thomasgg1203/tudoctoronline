//PRIMERA PARTE A LA HORA DE ESTAR CREANDOLO
var app = require(`./app`);//Direccion de app, ya que es el controlador de las configuraciones de cabezera y los cords
var mongoose=require(`./conexDB/conn`);//Conexion con base de datos.
var port = 4000;//Puerto que se usara
//SEGUNDA PARTE CUANDO SE ESTA CREANDO 
app.listen(port,()=>{
    console.log("Backend esta corriendo de forma correcta"); //Mensaje de consola, para ver si esta corriendo bien.
});