const {Router} = require (`express`);

const router = Router();
//Ruta para el controlador de cita.
var controllerCita = require (`../src/controllers/controllerCita`);
//Ruta para el controlador de paciente
var controllerPaciente = require('../src/controllers/controllerPaciente');
//Ruta para e controlador de administrador
var controllerAdministrador = require('../src/controllers/controllerAdministrador');
//Direcciones De Cita.
router.get(`/cita`,controllerCita.cita); //Prueba de que este corriendo bien el controlador cita
router.post(`/crearCita`,controllerCita.saveCita);
router.get(`/buscarCita/:id`,controllerCita.buscarData);
router.get(`/buscarCitaAll/:id?`,controllerCita.listarAllData);
router.put(`/actualizarCita/:id`,controllerCita.updateCita)
router.delete(`/eliminarCita/:id`,controllerCita.deleteCita);
//Direccion para Paciente.
router.get(`/paciente`, controllerPaciente.paciente);
router.post(`/crearPaciente`, controllerPaciente.savePaciente);
router.get(`/buscarPaciente/:id`, controllerPaciente.buscarPaciente);
router.get(`/buscarPacienteAll/:id?`, controllerPaciente.listarAllPaciente);
router.put(`/actualizarPaciente/:id`, controllerPaciente.updatePaciente);
router.delete(`/eliminarPaciente/:id`,controllerPaciente.deletePaciente);
//Direccion para Administrador
router.get(`/admin`, controllerAdministrador.administrador);
router.post(`/crearAdmin`, controllerAdministrador.saveAdministrador);
router.get(`/buscarAdmin/:id`, controllerAdministrador.buscarAdministrador);
router.get(`/buscarAdminAll/:id?`, controllerAdministrador.listarAllAdministrador);
router.put(`/actualizarAdmin/:id`, controllerAdministrador.updateAdministrador);
router.delete(`/eliminarAdmin/:id`, controllerAdministrador.deleteAdministrador);

module.exports=router;