const Administrador = require("../models/administrador");
//Funcion para ver si conecta con el controlador de administrador.
function administrador(req, res){
    res.status(200).send({
        message: 'Probando la ejecucion del controlador administrador'
    });
}
//Metodos del CRUD

//Metodo para guardar
function saveAdministrador(req, res){
    var myAdministrador = new Administrador(req.body);
    //Funcion promesa
    myAdministrador.save((err, result) =>{
        res.status(200).send({message:result});
    });
}
//Metodo para buscar de uno.
function buscarAdministrador(req, res){
    var idAdministrador = req.params.id;
    Administrador.findById(idAdministrador).exec((err, result)=>{
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en backend'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se pueden procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    });
}
//Metodo para buscar de a uno o todos los registros
function listarAllAdministrador(req, res){
    var idAdministrador = req.params.id;
    if(!idAdministrador){
        var result = Administrador.find({}).sort('origen');
    }else{
        var result = Administrador.find({_id:idAdministrador}).sort('origen');
    }
    result.exec(function(err, result){//Se usa exec como promesa
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en backend'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se pueden procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    });
}
//Metodo para eliminar
function deleteAdministrador(req, res){
    var idAdministrador = req.params.id;
    //metodo que provee mongoose
    Administrador.findByIdAndDelete(idAdministrador,function(err,Administrador){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado la administrador a eliminar'
            });
        }
        return res.json(Administrador);
    })
}
//Metodo para modificar
function updateAdministrador(req, res){
    var idAdministrador = req.params.id;
    //Metodo que provee moongose
    Administrador.findOneAndUpdate({_id: idAdministrador}, req.body,{new:true}, function(err, Administrador){
        if(err){
            res.send(err)
        }   
        res.json(Administrador)
    })
}

module.exports ={
    administrador,
    saveAdministrador,
    buscarAdministrador,
    listarAllAdministrador,
    deleteAdministrador,
    updateAdministrador
};