const { find, findOne, findOneAndUpdate } = require('../models/cita');
const Cita = require('../models/cita');
//Metodo para saber si el controlador esta en ejecucion
function cita(req, res){
    res.status(200).send({
        message:'Probando la ejecucion del controlador de cita'
    });
}
//Metodos del CRUD para la tabla Cita

//Funcion guardar
function saveCita(req, res){
    var myCita = new Cita(req.body);
    //TIPO PROMISE
    myCita.save((err, result)=>{
        res.status(200).send({message: result});
    });
}
//Funcion buscar por id en la cita.
function buscarData(req,res){
    var idCita =req.params.id;
    Cita.findById(idCita).exec((err,result)=>{
        if (err){
            res.status(500).send({message:`error al momento de procesar el backend`});
        }else{
            if(!result){
                res.status(404).send({message:`se ha ingresado los valores incorrectos y no se puede procesar`});
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}
//Para buscar uno o varios registros
function listarAllData(req,res){
    var idCita = req.params.id;
    if(!idCita){
        var result=Cita.find({}).sort(`origen`);
    }else{
        var result =Cita.find({_id:idCita}).sort(`origen`);
        }
        //PROMISE
        result.exec(function(err,result){
            if (err){
                res.status(500).send({message:`error al momento de procesar el backend`});
            }else{
                if(!result){
                    res.status(404).send({message:`se ha ingresado los valores incorrectos y no se puede procesar`});
                }else{
                    res.status(200).send({message:result});
                }
            }  
        })
    }
    //Actualizar la un registro
function updateCita(req,res){
    var idCita= req.params.id;
    Cita.findOneAndUpdate({_id:idCita},req.body,{new:true},function(err,Cita){
        if(err)
        res.send(err)
        res.json(Cita)
    });
}
//Eliminar datos de un registro
function deleteCita(req, res){
    var idCita = req.params.id;
    //metodo que provee mongoose
    Cita.findByIdAndDelete(idCita,function(err,Cita){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado la ruta a eliminar'
            });
        }
        return res.json(Cita);
    });
}
//Exporta todo para la parte de rutas.
module.exports = {
    cita, 
    saveCita,
    buscarData,
    listarAllData,
    updateCita,
    deleteCita
}
