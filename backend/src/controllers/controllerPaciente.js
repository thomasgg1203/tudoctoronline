const Paciente = require("../models/paciente");
//Funcion para ver si conecta con el controlador de paciente.
function paciente(req, res){
    res.status(200).send({
        message: 'Probando la ejecucion del controlador paciente'
    });
}
//Metodos del CRUD

//Metodo para guardar
function savePaciente(req, res){
    var myPaciente = new Paciente(req.body);
    //Funcion promesa
    myPaciente.save((err, result) =>{
        res.status(200).send({message:result});
    });
}
//Metodo para buscar de uno.
function buscarPaciente(req, res){
    var idPaciente = req.params.id;
    Paciente.findById(idPaciente).exec((err, result)=>{
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en backend'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se pueden procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    });
}
//Metodo para buscar de a uno o todos los registros
function listarAllPaciente(req, res){
    var idPaciente = req.params.id;
    if(!idPaciente){
        var result = Paciente.find({}).sort('origen');
    }else{
        var result = Paciente.find({_id:idPaciente}).sort('origen');
    }
    result.exec(function(err, result){//Se usa exec como promesa
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en backend'});
        }else{
            if(!result){
                res.status(404).send({message:'Se ha ingresado valores incorrectos y no se pueden procesar'});
            }else{
                res.status(200).send({message:result});
            }
        }
    });
}
//Metodo para eliminar
function deletePaciente(req, res){
    var idPaciente = req.params.id;
    //metodo que provee mongoose
    Paciente.findByIdAndDelete(idPaciente,function(err,Paciente){
        if(err){
            return res.json(500,{
                message: 'No se ha encontrado la paciente a eliminar'
            });
        }
        return res.json(Paciente);
    })
}
//Metodo para modificar
function updatePaciente(req, res){
    var idPaciente = req.params.id;
    //Metodo que provee moongose
    Paciente.findOneAndUpdate({_id: idPaciente}, req.body,{new:true}, function(err, Paciente){
        if(err){
            res.send(err)
        }   
        res.json(Paciente)
    })
}

module.exports ={
    paciente,
    savePaciente,
    buscarPaciente,
    listarAllPaciente,
    deletePaciente,
    updatePaciente
};