var mongoose = require(`mongoose`);
var schema = mongoose.Schema;
//Datos que se le ingresan desde el posman, para añadirlo a la database
var AdministradorSchema = mongoose.Schema ({
    nombre: String,
    apellido: String,
    cedula: Number,
    correo: String,
    contrasenia: String
});
//Se le coloca administradore en la direccion, ya que mongoDB le añade una s al final.
const Administrador = mongoose.model(`administradore`, AdministradorSchema);
module.exports = Administrador;