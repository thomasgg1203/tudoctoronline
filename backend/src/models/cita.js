var mongoose = require(`mongoose`);

var schema = mongoose.Schema;
var CitaSchema = mongoose.Schema({
    cita: String,
    prioridad:String, 
    descripcion: String,
    fecha: String,
    activo: Number
});
//Se le coloca cita en la direccion, ya que mongoDB le añade una s al final
const Cita = mongoose.model(`cita`,CitaSchema);
module.exports = Cita;