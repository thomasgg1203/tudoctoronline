var mongoose = require(`mongoose`);
var schema = mongoose.Schema;
//Datos que se le ingresan desde el posman, para añadirlo a la database
var PacienteSchema = mongoose.Schema ({
    nombre: String,
    apellido: String,
    cedula: Number,
    correo: String,
    contrasenia: String,
    telefono: String,
    fecha: String,
    alergia: String
});
//Se le coloca paciente en la direccion, ya que mongoDB le añade una s al final
const Paciente = mongoose.model(`paciente`,PacienteSchema);
module.exports = Paciente;